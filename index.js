// Require the necessary discord.js classes
const { Client, Intents } = require('discord.js');
const { token, sourcelink, filelink, password, codeword, archivelink } = require('./config.json');

// Create a new client instance
const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

// When the client is ready, run this code (only once)
client.once('ready', () => {
	console.log('Ready!');
});

client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;

	const { commandName } = interaction;
	const string = interaction.options.getString('password');
	console.log(`User: ${interaction.user.username} has used ${commandName} with option ${string}!\n`);

	if (commandName === 'ping') {
		await interaction.reply({ content: 'Pong!', ephemeral: true });
	} else if (commandName === 'file') {
		await interaction.reply({ content: filelink, ephemeral: true });
	} else if (commandName === 'source') {
		await interaction.reply({ content: sourcelink, ephemeral: true });
	} else if (commandName === 'archive') {
		await interaction.reply({ content: archivelink, ephemeral: true });
	} else if (commandName === 'solution') {
		if (string === password) {
			await interaction.reply({ content: 'Password accepted!\nContact my maker with the codeword: ' + codeword, ephemeral: true });
		} else {
			await interaction.reply({ content: 'Sorry, that password is incorrect', ephemeral: true });
		}
	}
});

// Login to Discord with your client's token
client.login(token);

