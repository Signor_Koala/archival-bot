const { SlashCommandBuilder } = require('@discordjs/builders');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { clientId, guildId, token } = require('./config.json');

const commands = [
	new SlashCommandBuilder().setName('ping').setDescription('Replies with pong!'),
	new SlashCommandBuilder().setName('file').setDescription('Super secret file'),
	new SlashCommandBuilder().setName('source').setDescription('View the source code!'),
	new SlashCommandBuilder().setName('archive').setDescription('Get the link to the latest archive of this server!'),
	new SlashCommandBuilder().setName('solution').setDescription('Get the answer!').addStringOption(option => option.setName('password').setDescription('Enter the password').setRequired(true)),
]
	.map(command => command.toJSON());

const rest = new REST({ version: '9' }).setToken(token);

rest.put(Routes.applicationGuildCommands(clientId, guildId), { body: commands })
	.then(() => console.log('Successfully registered application commands.'))
	.catch(console.error);

